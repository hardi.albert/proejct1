#ifndef DIAG_TYPES_H
#define DIAG_TYPES_H

#include "Std_Types.h"

typedef boolean (Diag_CommandType)(uint8 *Payload, uint8 PayloadLength, uint8 *Response);

typedef struct
{
	uint8 CommandID;
	Diag_CommandType *Command;
} Diag_TableNodeType;

#endif
