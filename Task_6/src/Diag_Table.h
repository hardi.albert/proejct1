#ifndef DIAG_TABLE_H
#define DIAG_TABLE_h

#include "Diag_Types.h"
#include "Diag_Commands.h"

extern const Diag_TableNodeType Diag_Table[DIAG_NR_OF_COMMANDS];

#endif
