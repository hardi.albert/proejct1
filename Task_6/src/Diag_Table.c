#include "Diag_Commands.h"
#include "Diag_Table.h"

const Diag_TableNodeType Diag_Table[DIAG_NR_OF_COMMANDS] = {
		{0x01, Diag_Sum},
		{0x02, Diag_Multiplication},
		{0x03, Diag_Maximum},
		{0x04, Diag_Minimum},
		{0x05, Diag_Average},
		{0x06, Diag_BitwiseOR},
		{0x07, Diag_BitwiseAND}
};
