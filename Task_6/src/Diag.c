#include "Diag.h"
#include "Diag_Types.h"
#include "Diag_Table.h"
#include "Diag_Commands.h"
#include <stdio.h>
#include <stdint.h>

void Diag_Handle(uint8 *RequestFrame, uint8 *ResponseFrame)
{
	boolean success = 1;
	boolean command = 1;
	uint8 i;

	if(RequestFrame == NULL_PTR || *(RequestFrame + 1) > 6 || *(RequestFrame + 1) < 1)
	{
		*ResponseFrame = DIAG_NEGATIVE_INVALID_REQUEST;
		success = 0;
	}

	if(success)
	{
		for(i = 0; i < DIAG_NR_OF_COMMANDS; i++)
		{
			if(RequestFrame[0] == Diag_Table[i].CommandID)
			break;
		}

		if(i == DIAG_NR_OF_COMMANDS)
		{
			*ResponseFrame = DIAG_NEGATIVE_COMMAND_NOT_FOUND;
			command = 0;
		}

		if(command)
		{
			if(Diag_Table[i].Command(RequestFrame + 2, RequestFrame[1], ResponseFrame))
			{
				*ResponseFrame = DIAG_POSITIVE;
			}
			else
			{
				*ResponseFrame = DIAG_NEGATIVE_COMMAND_ERROR;
			}
		}
	}
}
