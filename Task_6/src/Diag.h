#ifndef DIAG_H
#define DIAG_H

#include "Std_Types.h"

#define DIAG_NEGATIVE_INVALID_REQUEST 0xFF
#define DIAG_NEGATIVE_COMMAND_NOT_FOUND 0xFE
#define DIAG_NEGATIVE_COMMAND_ERROR 0xFD
#define DIAG_POSITIVE 0x00

void Diag_Handle(uint8 *RequestFrame, uint8 *ResponseFrame);

#endif
