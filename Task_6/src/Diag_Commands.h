#ifndef DIAG_COMMANDS_H
#define DIAG_COMMANDS_H
#include "Diag_Types.h"

#define DIAG_NR_OF_COMMANDS 7

boolean Diag_Sum(uint8 *Payload, uint8 PayloadLength, uint8 *Response);
boolean Diag_Multiplication(uint8 *Payload, uint8 PayloadLength, uint8 *Response);
boolean Diag_Maximum(uint8 *Payload, uint8 PayloadLength, uint8 *Response);
boolean Diag_Minimum(uint8 *Payload, uint8 PayloadLength, uint8 *Response);
boolean Diag_Average(uint8 *Payload, uint8 PayloadLength, uint8 *Response);
boolean Diag_BitwiseOR(uint8 *Payload, uint8 PayloadLength, uint8 *Response);
boolean Diag_BitwiseAND(uint8 *Payload, uint8 PayloadLength, uint8 *Response);

#endif
