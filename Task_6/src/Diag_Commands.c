#include "Diag_Commands.h"
#include <stdint.h>

boolean Diag_Sum(uint8 *Payload, uint8 PayloadLength, uint8 *Response)
{
	uint16 sum = 0;
	for(uint8 i = 0; i < PayloadLength; i++)
		sum = sum + Payload[i];

	if(sum > 0xFFFF)
		return 0;

	if(sum > 0xFF)
	{
		Response[1] = 2;
		Response[2] = (sum & 0xFF00) >> 8;
		Response[3] = sum & 0xFF;
	}
	else
	{
		Response[1] = 1;
		Response[2] = sum;
	}

	return 1;
}

boolean Diag_Multiplication(uint8 *Payload, uint8 PayloadLength, uint8 *Response)
{
	uint16 product = 1;
	for(uint8 i = 0; i < PayloadLength; i++)
		product = product * Payload[i];

	if(product > 0xFFFF)
	{
		return 0;
	}

	if(product > 0xFF)
	{
		Response[1] = 2;
		Response[2] = (product & 0xFF00) >> 8;
		Response[3] = product & 0xFF;
	}
	else
	{
		Response[1] = 1;
		Response[2] = product;
	}
		return 1;
}

boolean Diag_Maximum(uint8 *Payload, uint8 PayloadLength, uint8 *Response)
{
	uint8 max = Payload[0];

	for(uint8 i = 1; i < PayloadLength; i++)
	{
		if(Payload[i] > max)
			max = Payload[i];
	}

	Response[1] = 1;
	Response[2] = max;

	return 1;
}

boolean Diag_Minimum(uint8 *Payload, uint8 PayloadLength, uint8 *Response)
{
	uint8 min = Payload[0];

	for(uint8 i = 1; i < PayloadLength; i++)
	{
		if(Payload[i] < min)
			min = Payload[i];
	}

	Response[1] = 1;
	Response[2] = min;

	return 1;
}

boolean Diag_Average(uint8 *Payload, uint8 PayloadLength, uint8 *Response)
{
	uint16 sum = 0;

	for(uint8 i = 0; i < PayloadLength; i++)
		sum = sum + Payload[i];

	uint8 average = sum / PayloadLength;
	Response[1] = 1;
	Response[2] = average;

	return 1;
}

boolean Diag_BitwiseOR(uint8 *Payload, uint8 PayloadLength, uint8 *Response)
{
	uint8 x = Payload[0];

	for(uint8 i = 1; i < PayloadLength; i++)
		x |= Payload[i];

	Response[1] = 1;
	Response[2] = x;

	return 1;
}

boolean Diag_BitwiseAND(uint8 *Payload, uint8 PayloadLength, uint8 *Response)
{
	uint8 x = Payload[0];

	for(uint8 i = 1; i < PayloadLength; i++)
		x &= Payload[i];

	Response[1] = 1;
	Response[2] = x;

	return 1;
}
