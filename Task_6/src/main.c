#include <stdio.h>
#include "Diag.h"

int main ()
{
	uint8 requestFrame[8] = {0x03, 0x04, 0x01, 0x02, 0x09, 0x03, 0x00, 0x00};
	uint8 responseFrame[4] = {0};

	Diag_Handle(requestFrame,responseFrame);

	printf("RequestFrame = {");
	for(uint8 i = 0; i < 8; i++)
	{
		if(i <= requestFrame[1] + 1 && i != 7)
		{
			printf("%02X, ", requestFrame[i]);
		}
		else if(i == requestFrame[1] + 1 && i == 7)
		{
			printf("%02X ",requestFrame[i]);
		}
		else if(i != 7)
		{
			printf("X, ");
		}
		else
		{
			printf("X");
		}
	}

	printf("}\n");

	printf("ResponseFrame = {");
	if(responseFrame[1] == 0)
	{
		printf("%02X, X, X, X",responseFrame[0]);
	}
	else
	{
		for(uint8 i = 0; i < 4; i++)
		{
			if(i <= responseFrame[1] + 1 && i != 3)
			{
				printf("%02X, ", responseFrame[i]);
			}
			else if(i == responseFrame[1] + 1 && i == 3)
			{
				printf("%02X ",responseFrame[i]);
			}
			else if(i != 3)
			{
				printf("X, ");
			}
			else
			{
				printf("X");
			}
		}
	}

	printf("}\n");

	return 0;
}
