#include "RingBuffer.h"
#include <stdio.h>

static RingBuffer_NodeType ringBuffer[RINGBUFFER_LENGTH];
static uint8 insertPos;
static uint8 removePos;
uint8 x = 0;

void RingBuffer_Init(void)
{
	for(uint8 i = 0; i < RINGBUFFER_LENGTH; i++)
	{
		ringBuffer[i] = RINGBUFFER_INIT_VALUE;
	}

	insertPos = 0;
	removePos = 0;
}

void RingBuffer_Insert(RingBuffer_NodeType Node)
{
	ringBuffer[insertPos] = Node;
	x++;
	insertPos = (insertPos + 1) % RINGBUFFER_LENGTH;
	if(x > RINGBUFFER_LENGTH)
	{
		removePos = (removePos + 1) % RINGBUFFER_LENGTH;
		x = RINGBUFFER_LENGTH;
	}
}

void RingBuffer_Remove(void)
{
	ringBuffer[removePos] = RINGBUFFER_INIT_VALUE;
	removePos = (removePos + 1) % RINGBUFFER_LENGTH;
	x--;
}

void RingBuffer_Print(void)
{
	for(uint8 i = 0; i < RINGBUFFER_LENGTH; i++)
	{
		printf("%d ", ringBuffer[i]);
	}
	printf("\n");
}
