#include "Stack.h"
#include "RingBuffer.h"
#include <stdio.h>

int main()
{
	// STACK TEST
	Stack_Print(); // "Empty"
	Stack_Push(4);
	Stack_Push(2);
	Stack_Print(); // "4 2"

	Stack_Push(7);
	Stack_Pop();
	Stack_Push(3);
	Stack_Print(); // "4 2 3"

	Stack_Push(0);
	Stack_Print(); // "4 2 3 0"

	Stack_Push(8);
	Stack_Print(); // "4 2 3 0"

	Stack_Pop();
	Stack_Pop();
	Stack_Pop();
	Stack_Print(); // "4"

	Stack_Pop();
	Stack_Print(); // "Empty"
	Stack_Push(10);
	Stack_Push(5);
	Stack_Pop();
	Stack_Pop();
	Stack_Pop();
	Stack_Print(); // "Empty" --- all good


	// RINGBUFFER TEST
	RingBuffer_Init();
	RingBuffer_Print(); // “0 0 0 0 0”
	RingBuffer_Insert(1);
	RingBuffer_Insert(2);
	RingBuffer_Insert(3);
	RingBuffer_Print(); // “1 2 3 0 0”
	RingBuffer_Insert(4);
	RingBuffer_Insert(5);
	RingBuffer_Insert(6);
	RingBuffer_Print(); // “6 2 3 4 5”
	RingBuffer_Insert(7);
	RingBuffer_Remove();
	RingBuffer_Remove();
	RingBuffer_Print(); // “6 7 0 0 5”
	RingBuffer_Insert(8);
	RingBuffer_Remove();
	RingBuffer_Print(); // “6 7 8 0 0”
	RingBuffer_Remove();
	RingBuffer_Remove();
	RingBuffer_Remove();
	RingBuffer_Print(); // “0 0 0 0 0”
	RingBuffer_Remove();
	RingBuffer_Remove();
	RingBuffer_Print(); // “0 0 0 0 0”
	RingBuffer_Insert(9);
	RingBuffer_Print(); // “0 0 0 9 0”

	return 0;
}
