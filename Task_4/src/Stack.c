#include "Stack.h"
#include <stdio.h>

static Stack_NodeType stack[STACK_LENGTH];
static int top = -1;

boolean Stack_Push(Stack_NodeType Node)
{
	if(top >= STACK_LENGTH - 1)
		return 0; // overflow

	top++;
	stack[top] = Node;
	return 1;
}

boolean Stack_Pop(void)
{
	if(top < 0)
		return 0;

	top--;
	return 1;
}

void Stack_Print(void)
{
	if(top < 0)
	{
		printf("Empty\n");
		return;
	}
	for(int i=0; i<=top; i++)
	{
		printf("%u ",stack[i]);
	}
	printf("\n");
}
