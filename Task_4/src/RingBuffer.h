#include "RingBuffer_Cfg.h"

void RingBuffer_Init(void);
void RingBuffer_Insert(RingBuffer_NodeType Node);
void RingBuffer_Remove(void);
void RingBuffer_Print(void);
