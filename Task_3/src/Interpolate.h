#include "Std_Types.h"

typedef struct
{
	sint8 x;
	sint8 y;
} Point_Type;

sint8 Interpolate(sint8 x, Point_Type A, Point_Type B);
