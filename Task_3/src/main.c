#include <stdio.h>
#include "Interpolate.h"
#include "WriteComplexSum.h"

int main ()
{
	Point_Type A = {3 ,3};
	Point_Type B = {3 ,4};
	sint8 x = 4;
	sint8 interpolated = Interpolate(x,A,B);
	if(interpolated != -128)
		printf("Interpolated value at %d: %d\n\n",x,interpolated);
	else
		printf("Invalid interpolation \n\n");

	ComplexNumber_Type a = {1,1,1};
	ComplexNumber_Type b = {2,1,2};
	WriteComplexSum(a,b);

	return 0;
}
