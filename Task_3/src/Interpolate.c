#include "Interpolate.h"

sint8 Interpolate(sint8 x, Point_Type A, Point_Type B)
{
	if(x == A.x)
	{
		printf("Invalid x agrument (division by 0)\n");
		return -128;
	}
	if(A.x == B.x)
	{
		printf("Invalid arguments (division by 0)\n");
		return -128;
	}

	sint8 y = ((B.y - A.y) * (x - A.x)) / (B.x - A.x) + A.y;

	if(y < -128 || y > 127) // underflow/overflow
	{
		return -128;
	}

	return y;
}

// A.x == x0	A.y == y0
// B.x == x1	B.y == y1
