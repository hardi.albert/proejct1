#include "WriteComplexSum.h"
#include <stdio.h>

void WriteComplexSum(ComplexNumber_Type A, ComplexNumber_Type B)
{
	sint8 powA = A.iPow;
	sint8 powB = B.iPow;
	boolean Ahasi = 1;
	boolean Bhasi = 1;

	if(powA % 4 == 0) // i^4 = 1
		{
			A.b = A.b*1;
			Ahasi = 0;
			A.a = A.a + A.b;
			A.b = 0;
		}
		else if(powA % 2 == 0) // i^2 = -1
			{
				A.b = A.b * -1;
				Ahasi = 0;
				A.a = A.a + A.b;
				A.b = 0;
			}
		else if(powA % 3 == 0)  // i^3 = -i
			{
				A.b = A.b * -1;
				Ahasi = 1;
			}
		else if(powA % 1 == 0) // i^1 = i
			{
				Ahasi = 1;
			}
		else if(powA == 0) // i^0 = 1
			{
				A.b = A.b * 1;
			}

		if(A.a == 0 && Ahasi)
			printf("A = %di\n",A.b);
		else if(A.b == 0)
			printf("A = %d\n",A.a);
		else if(A.a != 0 && A.b > 0 && Ahasi)
			printf("A = %d+%di\n",A.a,A.b);
		else if(A.a != 0 && A.b != 0 && Ahasi)
			printf("A = %d%di\n",A.a,A.b);
		else if(A.a != 0 && A.b != 0)
			printf("A = %d\n",A.a + A.b);
		else
			printf("A = 0\n");


	if(powB % 4 == 0) // i^4 = 1
	{
		B.b = B.b*1;
		Bhasi = 0;
		B.a = B.a + B.b;
		B.b = 0;
	}
	else if(powB % 2 == 0) // i^2 = -1
		{
			B.b = B.b * -1;
			Bhasi = 0;
			B.a = B.a + B.b;
			B.b = 0;
		}
	else if(powB % 3 == 0)  // i^3 = -i
		{
			B.b = B.b * -1;
			Bhasi = 1;
		}
	else if(powB % 1 == 0) // i^1 = i
		{
			Bhasi = 1;
		}
	else if(powB == 0) // i^0 = 1
		{
			B.b = B.b * 1;
		}

	if(B.a == 0 && Bhasi)
		printf("B = %di\n",B.b);
	else if(B.b == 0)
		printf("B = %d\n",B.a);
	else if(B.a != 0 && B.b > 0 && Bhasi)
		printf("B = %d+%di\n",B.a,B.b);
	else if(B.a != 0 && B.b != 0 && Bhasi)
		printf("B = %d%di\n",B.a,B.b);
	else if(B.a != 0 && B.b != 0)
		printf("B = %d\n",B.a + B.b);
	else
		printf("B = 0\n");

	sint8 imag = A.b + B.b;
	sint8 real = A.a + B.a;

	printf("The sum of the complex number is: ");
	if(real != 0 && imag > 0 && (Ahasi || Bhasi))
		printf("%d+%di",real,imag);
	else if(real != 0 && imag != 0 && (Ahasi ||Bhasi))
		printf("%d%di",real,imag);
	else if(real != 0 && imag == 0)
		printf("%d",real);
	else if(real == 0 && imag != 0 && (Ahasi || Bhasi))
		printf("%di",imag);
	else
		printf("0");

}
