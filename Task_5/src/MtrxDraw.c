#include "MtrxDraw.h"
#include "MtrxDraw_Cfg.h"
#include <stdio.h>

uint8 Matrix[MTRXDRAW_LENGTH][MTRXDRAW_LENGTH];

void MtrxDraw_Square(uint8 Row, uint8 Col, uint8 Length, uint8 Fill)
{
	if(Row + Length > MTRXDRAW_LENGTH || Col + Length > MTRXDRAW_LENGTH)
		{
			printf("Invalid parameters\n");
			return;
		}

	for(uint8 i = Row; i < Row + Length; i++)
		for(uint8 j = Col; j < Col + Length; j++)
			Matrix[i][j] = Fill;
}

void MtrxDraw_Print(uint8 Row, uint8 Col, uint8 Length)
{
	if( Row + Length > MTRXDRAW_LENGTH || Col + Length > MTRXDRAW_LENGTH)
		{
			printf("Invalid parameters\n");
			return;
		}

	for(uint8 i = Row; i < Row + Length; i++) {
		for(uint8 j = Col; j < Col + Length; j++) {
			printf("%c", Matrix[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

