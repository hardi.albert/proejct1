#include "MtrxSum.h"
#include "MtrxSum_Cfg.h"

MtrxSum_Type MtrxSum_Calculate(void)
{
	uint8 maxSum = 0;
	MtrxSum_Type result = MTRXSUM_PRIMARY_DIAGONAL;

	uint8 primaryDiagonalSum = 0;
	uint8 secondaryDiagonalSum = 0;
	uint8 firstRowSum = 0;
	uint8 lastRowSum = 0;
	uint8 firstColumnSum = 0;
	uint8 lastColumnSum = 0;

	for(uint8 i = 0; i < MTRXSUM_LENGTH; i++)
	{
		primaryDiagonalSum += MtrxSum_Target[i][i];
		secondaryDiagonalSum += MtrxSum_Target[i][MTRXSUM_LENGTH - i - 1];
		firstRowSum += MtrxSum_Target[0][i];
		lastRowSum += MtrxSum_Target[MTRXSUM_LENGTH - 1][i];
		firstColumnSum += MtrxSum_Target[i][0];
		lastColumnSum += MtrxSum_Target[i][MTRXSUM_LENGTH - 1];
	}

	if(primaryDiagonalSum > maxSum)
	{
		maxSum = primaryDiagonalSum;
		result = MTRXSUM_PRIMARY_DIAGONAL;
	}

	if(secondaryDiagonalSum > maxSum)
		{
			maxSum = secondaryDiagonalSum;
			result = MTRXSUM_SECONDARY_DIAGONAL;
		}

	if(firstRowSum > maxSum)
		{
			maxSum = firstRowSum;
			result = MTRXSUM_FIRST_ROW;
		}

	if(lastRowSum > maxSum)
			{
				maxSum = lastRowSum;
				result = MTRXSUM_LAST_ROW;
			}

	if(firstColumnSum > maxSum)
			{
				maxSum = firstColumnSum;
				result = MTRXSUM_FIRST_COLUMN;
			}

	if(lastColumnSum > maxSum)
				{
					maxSum = lastColumnSum;
					result = MTRXSUM_LAST_COLUMN;
				}

	return result;
}
