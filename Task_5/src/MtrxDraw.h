#include "Std_Types.h"
#include "MtrxDraw_Cfg.h"

uint8 Matrix[MTRXDRAW_LENGTH][MTRXDRAW_LENGTH];

void MtrxDraw_Square(uint8 Row, uint8 Col, uint8 Length, uint8 Fill);
void MtrxDraw_Print(uint8 Row, uint8 Col, uint8 Length);
