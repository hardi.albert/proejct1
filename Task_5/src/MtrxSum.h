#include "Std_Types.h"

typedef enum {

	MTRXSUM_PRIMARY_DIAGONAL = 0,
	MTRXSUM_SECONDARY_DIAGONAL = 1,
	MTRXSUM_FIRST_ROW = 2,
	MTRXSUM_LAST_ROW = 3,
	MTRXSUM_FIRST_COLUMN = 4,
	MTRXSUM_LAST_COLUMN = 5,

} MtrxSum_Type;

MtrxSum_Type MtrxSum_Calculate(void);
