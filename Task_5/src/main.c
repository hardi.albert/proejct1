#include <stdio.h>
#include "MtrxSum.h"
#include "MtrxDraw.h"

int main ()
{
	// MtrxSum TEST -- good
	MtrxSum_Type result = MtrxSum_Calculate();
	printf("%d\n",result);

	// MtrxDraw TEST -- good
	for(uint8 i = 0; i < MTRXDRAW_LENGTH; i++)
		for(uint8 j = 0; j < MTRXDRAW_LENGTH; j++)
			Matrix[i][j] = '#';

	MtrxDraw_Print(0,0, MTRXDRAW_LENGTH);

	MtrxDraw_Square(2,1,4,'@');
	MtrxDraw_Print(0,0, MTRXDRAW_LENGTH);

	MtrxDraw_Square(0, 3, 3, '.');
	MtrxDraw_Print(0,0, MTRXDRAW_LENGTH);

	MtrxDraw_Square(0, 5, 1, '§');
	MtrxDraw_Print(0,0, MTRXDRAW_LENGTH);

	MtrxDraw_Print(2,4,2);
	MtrxDraw_Print(0,0,3);
	MtrxDraw_Print(0,1,5);
	MtrxDraw_Print(0,0,6);

return 0;
}
