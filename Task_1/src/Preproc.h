#include "Preproc_Cfg.h"
#include <stdio.h>

#if (PREPROC_TASK_API_ENABLE == STD_ON)
	#if	((PREPROC_TASK1_ACTIVATION == STD_OFF) && \
		(PREPROC_TASK2_ACTIVATION == STD_OFF) && \
		(PREPROC_TASK3_ACTIVATION == STD_OFF))
		#error "Task API is enabled but no task is activated.\n"
	#elif	(((PREPROC_TASK1_ACTIVATION == STD_ON) || \
			(PREPROC_TASK2_ACTIVATION == STD_ON) || \
			(PREPROC_TASK3_ACTIVATION == STD_ON)) && \
			((PREPROC_TASK1_ACTIVATION == STD_OFF) || \
			(PREPROC_TASK2_ACTIVATION == STD_OFF) || \
			(PREPROC_TASK3_ACTIVATION == STD_OFF)))
		#warning "Task API is enabled but at least one task is not activated.\n"

	#elif 	((PREPROC_TASK1_ID == PREPROC_TASK2_ID) || \
			(PREPROC_TASK1_ID == PREPROC_TASK3_ID) || \
			(PREPROC_TASK2_ID == PREPROC_TASK3_ID) || \
			(PREPROC_TASK1_ID > 255) || \
			(PREPROC_TASK2_ID > 255) || \
			(PREPROC_TASK3_ID > 255))
		#error "Task IDs shall be different and contain a value between 0 and 255.\n"
	#endif

#define PREPROC_TASK_IMPL(taskId) \
	void task_##taskId() { \
		printf("Task ID: %d\n",taskId);}

#define PREPROC_TASK_CALL(taskId) \
	task_##taskId();


#else

	#define PREPROC_TASK_IMPL(taskId) \
		printf("Task API is deactivated.\n");
	#define PREPROC_TASK_CALL(taskId) \
		printf("Task API is deactivated.\n");
#endif
