#include "VerifyPalindrome.h"

boolean VerifyPalindrome(uint8 Number)
{
	uint8 reversed_number = 0;
	uint8 original_number = Number;

	while(original_number > 0)
	{
		reversed_number <<= 1;
		reversed_number |= (original_number & 1);
		original_number >>= 1;
	}

	if(reversed_number == Number)
		return 1;
	else
		return 0;
}
