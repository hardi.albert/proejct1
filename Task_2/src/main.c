#include <stdio.h>
#include "VerifyPalindrome.h"
#include "VerifyPowerOf2.h"
#include "CalculateSumOfNibbles.h"

int main ()
{
	uint8 number1 = 33;

	if(VerifyPalindrome(number1))
		printf("%u is palindrome\n",number1);
	else
		printf("%u is NOT palindrome\n",number1);

	uint8 number2 = 16;

	if(VerifyPowerOf2(number2))
		printf("%u is a power of 2\n",number2);
	else
		printf("%u is NOT a power of 2\n",number2);

	sint8 number3 = 0xB1;
	sint8 sum = CalculateSumOfNibbles(number3);

	printf("The sum of low and high nibbles are: %d\n",sum);

	return 0;
}
