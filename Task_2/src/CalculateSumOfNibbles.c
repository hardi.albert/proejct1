#include "CalculateSumOfNibbles.h"

sint8 CalculateSumOfNibbles(uint8 Byte)
{
	sint8 high = (sint8)(Byte >> 4);
	uint8 low = Byte & 0x0F;

	if(high & 0x08)
	{
		high = -((high-1) ^ 0x0F);
	}

	sint8 sum = high + (sint8)low;
	return sum;
}
