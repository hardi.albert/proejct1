#include "VerifyPowerOf2.h"

boolean VerifyPowerOf2(uint8 Number)
{
	uint8 nr = 0;
	uint8 number = Number;

	while(number > 0)
	{
		if(number & 1)
			nr++;
		number >>= 1;
	}

	if(nr == 1)
		return 1;
	else
		return 0;
}
